package com.example.wisdom.exception;

/**
 * @description: 基础的接口类，自定义的错误描述枚举类需实现该接口
 * @author: liupeng
 * @date: 2020/4/30 9:10
 */
public interface BaseErrorInfoInterface {
    /**
     * @description: 错误码
     * @param: null
     * @author: liupeng
     * @date: 2020/4/30 10:52
     */
    String getResultCode();

    /**
     * @description: 错误描述
     * @param: null
     * @return: String
     * @author: liupeng
     * @date: 2020/4/30 10:52
     */
    String getResultMsg();
}
