package com.example.wisdom.web;

import com.example.wisdom.config.ResultBody;
import com.example.wisdom.service.LeavingCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


/**
 * @description: 留言分类api
 * @author: liupeng
 * @date: 2020/4/28 15:54
 */
@Controller
@RequestMapping(value = "/api")
public class LeavingCategoryController {
    @Autowired
    private LeavingCategoryService leavingCategoryService;

    @GetMapping("/getCategoryList")
    @ResponseBody
    public ResultBody getCategoryList(@PageableDefault(size = 9)  Pageable pageable) {
        return leavingCategoryService.list(pageable);
    }

    @GetMapping("/getCategoryReport")
    @ResponseBody
    public ResultBody getCategoryReport() {
        return leavingCategoryService.list();
    }

}
