package com.example.wisdom.web;

import com.example.wisdom.config.ResultBody;
import com.example.wisdom.service.LeavingMessageService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


/**
 * @description: 留言列表接口
 * @author: liupeng
 * @date: 2020/4/28 15:55
 */
@Controller
@RequestMapping(value = "/api")
public class LeavingMessageController {

    @Autowired
    private LeavingMessageService leavingMessageService;

    @GetMapping("/getMessageList")
    @ResponseBody
    public ResultBody getMessageList(@RequestParam(value = "messageSubject", defaultValue = "") String messageSubject, @PageableDefault(size = 10, sort = {"messageTime"}) Pageable pageable) {

        return leavingMessageService.list(messageSubject, pageable);
    }

    @GetMapping("/getMessageList/pageable/{categoryId}")
    @ResponseBody
    public ResultBody getMessageList(@PathVariable("categoryId") Long categoryId, @PageableDefault(size = 10, sort = {"messageTime"}) Pageable pageable) {
        return leavingMessageService.list(categoryId, pageable);
    }

    @GetMapping("/getMessageList/{id}")
    @ResponseBody
    public ResultBody getMessageDetail(@PathVariable("id") Long id) {
        return leavingMessageService.findById(id);
    }

    @PutMapping("/getMessageList/addLikeNum/{id}")
    @ResponseBody
    public ResultBody addLeavingMessageLikeNumById(@PathVariable("id") Long id) {
        return leavingMessageService.updateCategoryLikeNumById(id);
    }

    @PutMapping("/getMessageList/addOppositionNum/{id}")
    @ResponseBody
    public ResultBody addLeavingMessageOppositionNumById(@PathVariable("id") Long id) {
        return leavingMessageService.updateCategoryOppositionNumById(id);
    }

    @PutMapping("/getMessageList/ReplyOpinion/{id}")
    @ResponseBody
    public ResultBody addLeavingMessageReplyOpinionById(@PathVariable("id") Long id, String replyOpinion) {
        return leavingMessageService.updateCategoryReplyOpinionById(id, replyOpinion);
    }

    @ResponseBody
    @PostMapping("/upload")
    @ApiOperation("读表格")
    public ResultBody file(MultipartFile file) throws Exception {
        ResultBody resultBody = new ResultBody();

        resultBody.setResult(leavingMessageService.getExcel(file));

        return resultBody;
    }
}
