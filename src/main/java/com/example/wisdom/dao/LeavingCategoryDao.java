package com.example.wisdom.dao;

import com.example.wisdom.entity.LeavingCategory;
import com.example.wisdom.entity.LeavingMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.lang.NonNull;

import java.util.List;
import java.util.Optional;

/**
 * @description: DAO LeavingCategoryDao
 * @author: longxin
 * @date: 2020/4/30 10:27
 */
public interface LeavingCategoryDao extends JpaRepository<LeavingCategory, Long>, JpaSpecificationExecutor<LeavingCategory> {

    /**
     * this a findByCategoryName
     * @param categoryName
     * @return List<LeavingCategory>
     */
    List<LeavingCategory> findByCategoryName(String categoryName);
}
