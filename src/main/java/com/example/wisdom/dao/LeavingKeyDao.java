package com.example.wisdom.dao;


import com.example.wisdom.entity.LeavingKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;
/**
 * @description: LeavingKeyDao
 * @author: longxin
 * @date: 2020/4/30 10:35
 */
public interface LeavingKeyDao extends JpaRepository<LeavingKey, Long>, JpaSpecificationExecutor<LeavingKey> {

    /**
     * findLeavingKeysByLeavingCategoryId by rule id
     *
     * @param id
     * @return Set<LeavingKey>
     */
    Set<LeavingKey> findLeavingKeysByLeavingCategoryId(Long id);

    /**
     * findByKeyName by rule name
     *
     * @param keyName
     * @return List<LeavingKey>
     */
    List<LeavingKey> findByKeyName(String keyName);

    /**
     * findByKeyName by rule name
     *
     * @param LeavingCategoryId
     * @return void
     */
    @Modifying
    @Transactional
    @Query("delete from LeavingKey l where l.leavingCategoryId = ?1")
    void deleteAllByLeavingCategoryId(Long LeavingCategoryId);
}
