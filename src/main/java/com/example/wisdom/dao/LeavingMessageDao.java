package com.example.wisdom.dao;

import com.example.wisdom.entity.LeavingMessage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;

public interface LeavingMessageDao extends JpaRepository<LeavingMessage, Long>, JpaSpecificationExecutor<LeavingMessage> {

    /**
     * @description: updateisCategory
     * @param: null
     * @return:
     * @date: 2020/4/30 10:35
     */
    @Modifying
    @Transactional
    @Query("update LeavingMessage l set l.isCategory = 0")
    void updateisCategory();

    /**
     * @description: updateCategory
     * @param: null
     * @return:
     * @date: 2020/4/30 10:35
     */
    @Modifying
    @Transactional
    @Query("update LeavingMessage l set l.isCategory = 0, l.leavingCategoryId = null")
    void updateCategory();

    @Modifying
    @Transactional
    @Query("update LeavingMessage l set l.leavingCategoryId = ?2 , l.isCategory = 1 where l.id = ?1")
    void updateCategory(Long id, Long categoryId);

    /**
     * @description: findByisCategory
     * @param: null
     * @return:
     * @date: 2020/4/30 10:35
     */
    List<LeavingMessage> findByisCategory(boolean isCategory);

    /**
     * @description: findByisCategory
     * @param: null
     * @return:
     * @date: 2020/4/30 10:35
     */
    Long countByLeavingCategoryIdAndIsCategory(Long id, boolean isCategory);


    Page<LeavingMessage> findByMessageSubjectIsLike(String messageSubject,Pageable pageable);
}
