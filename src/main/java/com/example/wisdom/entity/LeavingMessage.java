package com.example.wisdom.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;


@Data
@Entity
@Table(name = "t_leaving_message")
public class LeavingMessage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String userName;

    private String messageNumber;

    private String messageSubject;

    @Temporal(TemporalType.TIMESTAMP)
    private Date messageTime;

    @Basic(fetch = FetchType.LAZY)
    @Lob
    private String messageDetail;

    private Boolean isCategory;

    private Integer likeNumber;

    private Integer oppositionNumber;

    @Basic(fetch = FetchType.LAZY)
    @Lob
    private String replyOpinion;

    @Temporal(TemporalType.TIMESTAMP)
    private Date replyTime;

    private Long leavingCategoryId;

}
