package com.example.wisdom.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "t_leaving_key")
public class LeavingKey {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String keyName;

    private Long leavingCategoryId;
}
