package com.example.wisdom.util;

import java.util.*;

/**
 * @description: KWUtil
 * @author: longxin
 * @date: 2020/4/30 10:50
 */
public class KWUtil {

    public static int checkWord(String text, int beginIndex, Set<String> set) {
        if (set == null) {
            throw new RuntimeException("字典不能为空！");
        }
        Map<String, Object> map = handleToMap(set);
        boolean isEnd = false;
        int wordLength = 0;
        Map<String, Object> curMap = map;
        int len = text.length();
        for (int i = beginIndex; i < len; i++) {
            String key = String.valueOf(text.charAt(i));
            curMap = (Map<String, Object>) curMap.get(key);
            if (curMap == null) {
                break;
            }
            else {
                wordLength++;
                if ("1".equals(curMap.get("isEnd"))) {
                    isEnd = true;
                }
            }
        }
        if (!isEnd) {
            wordLength = 0;
        }
        return wordLength;
    }

    public static List<String> getWords(String text, Set<String> set) {
        List<String> wordSet = new ArrayList<>();
        int len = text.length();
        List<Integer> integers = new ArrayList<>();
        for (int i = 0; i < len; i++) {
            int wordLength = checkWord(text, i, set);
            integers.add(wordLength);
            if (wordLength > 0) {
                String word = text.substring(i, i + wordLength);
                wordSet.add(word);
                i = i + wordLength - 1;
            }
        }

        return wordSet;
    }


    private static Map<String, Object> handleToMap(Set<String> wordSet) {
        if (wordSet == null) {
            return null;
        }
        Map<String, Object> map = new HashMap<String, Object>(wordSet.size());
        Map<String, Object> curMap = null;
        Iterator<String> ite = wordSet.iterator();
        while (ite.hasNext()) {
            String word = ite.next();
            curMap = map;
            int len = word.length();
            for (int i = 0; i < len; i++) {
                String key = String.valueOf(word.charAt(i));
                Map<String, Object> wordMap = (Map<String, Object>) curMap
                        .get(key);
                if (wordMap == null) {
                    wordMap = new HashMap<String, Object>();
                    wordMap.put("isEnd", "0");
                    curMap.put(key, wordMap);
                    curMap = wordMap;
                }
                else {
                    curMap = wordMap;
                }
                if (i == len - 1) {
                    curMap.put("isEnd", "1");
                }
            }
        }
        return map;
    }
}
