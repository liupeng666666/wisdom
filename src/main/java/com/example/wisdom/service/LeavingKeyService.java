package com.example.wisdom.service;

import com.example.wisdom.entity.LeavingKey;

import java.util.List;
import java.util.Set;
/**
 * @description: LeavingKeyService
 * @author: longxin
 * @date: 2020/4/30 10:51
 */
public interface LeavingKeyService {

    List<LeavingKey> getAll();

    /**
     * 在已知字典中获取对应类别的关键字字典
     * @param id
     * @param leavingKeys
     * @return
     */
    List<LeavingKey> getLeavingKeysByCategoryId(Long id, List<LeavingKey> leavingKeys);

    /**
     * 在数据库中获取对应类别的关键字字典
     * @param id
     * @return
     */
    Set<LeavingKey> getLeavingKeysByCategoryId(Long id);

    /**
     * 在数据库中获取对应类别的关键字字典
     * @param id
     * @return
     */
    Set<String> getKeysByCategoryId(Long id);

    /**
     * 在已知字典中获取对应类别的关键字字典
     * @param id
     * @param leavingKeys
     * @return
     */
    Set<String> getKeysByCategoryId(Long id, List<LeavingKey> leavingKeys);

    /**
     *
     * @param categoryId
     * @param key
     */
    void saveKey(Long categoryId, String...key);

    /**
     * 清空所有分类字典
     */
    void deleteCategory();

    /**
     * 根据分类id删除分类的字典
     * @param categoryId
     */
    void deleteCategory(Long categoryId);
}
