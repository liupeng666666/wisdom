package com.example.wisdom.service;

import com.example.wisdom.config.ResultBody;
import com.example.wisdom.entity.LeavingCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
/**
 * @description: LeavingCategoryService
 * @author: longxin
 * @date: 2020/4/30 10:51
 */
public interface LeavingCategoryService {

    /**
     * @description: 获取分页列表
     * @param: pageable
     */
    ResultBody list(Pageable pageable);

    /**
     * @description: 获取所有列表
     * @param: pageable
     */
    ResultBody list();
    /**
     * 获取所有字典
     * @return
     */
    List<LeavingCategory> getAll();

    /**
     * 根据id获取
     * @param id
     * @return
     */
    LeavingCategory getLeavingById(Long id);

    /**
     * 根据分类名称获取分类
     * @param categoryName
     * @return
     */
    List<LeavingCategory> getCategoryByName(String categoryName);

    /**
     * 根据分类名称精准获取分类id
     * @param categoryName
     * @return
     */
    Long getCategoryIdByName(String categoryName);


    /**
     * 更新分类
     * @param
     * @return
     */
    void updateCategoryById(LeavingCategory leavingCategory);
}
