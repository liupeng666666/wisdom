package com.example.wisdom.service.impl;

import com.example.wisdom.config.ResultBody;
import com.example.wisdom.dao.LeavingCategoryDao;
import com.example.wisdom.entity.LeavingCategory;
import com.example.wisdom.service.LeavingCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @description: LeavingCategoryServiceImpl
 * @author: liupeng
 * @date: 2020/4/30 10:49
 */
@Service
public class LeavingCategoryServiceImpl implements LeavingCategoryService {



    @Autowired
    private LeavingCategoryDao leavingCategoryDao;

    /**
     * @param pageable
     * @description: 获取分页列表
     * @param: pageable
     */
    @Override
    public ResultBody list(Pageable pageable) {
        return ResultBody.success(leavingCategoryDao.findAll(pageable));
    }

    @Override
    public ResultBody list() {
        return ResultBody.success(leavingCategoryDao.findAll());
    }

    @Override
    public List<LeavingCategory> getAll() {
        return leavingCategoryDao.findAll();
    }

    @Override
    public LeavingCategory getLeavingById(Long id) {
        return leavingCategoryDao.findById(id).get();
    }

    @Override
    public List<LeavingCategory> getCategoryByName(String categoryName) {
        return leavingCategoryDao.findByCategoryName(categoryName);
    }

    @Override
    public Long getCategoryIdByName(String categoryName) {
        List<LeavingCategory> leavingCategories = leavingCategoryDao.findByCategoryName(categoryName);
        if (leavingCategories.size() > 1) {
            return null;
        }
        return leavingCategories.get(0).getId();
    }

    public void updateCategoryById(LeavingCategory leavingCategory){
        leavingCategoryDao.save(leavingCategory);
    }
}
