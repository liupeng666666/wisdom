package com.example.wisdom.service.impl;

import com.example.wisdom.dao.LeavingMessageDao;
import com.example.wisdom.entity.LeavingCategory;
import com.example.wisdom.entity.LeavingMessage;
import com.example.wisdom.service.LeavingCategoryService;
import com.example.wisdom.service.LeavingKeyService;
import com.example.wisdom.service.LeavingMessageService;
import com.example.wisdom.service.ManageService;
import com.example.wisdom.util.KWUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * @description: ManageServiceImpl
 * @author: liupeng
 * @date: 2020/4/30 10:48
 */
@Service
public class ManageServiceImpl implements ManageService {

    @Autowired
    private LeavingKeyService leavingKeyService;

    @Autowired
    private LeavingCategoryService leavingCategoryService;

    @Autowired
    private LeavingMessageService leavingMessageService;

    @Autowired
    private LeavingMessageDao leavingMessageDao;

    @Override
    public void setCategory() {
        this.setCategory(leavingCategoryService.getAll(), leavingMessageService.getNotCategory());
    }

    @Override
    public void setAllCategory() {
        this.setCategory(leavingCategoryService.getAll(), leavingMessageService.getAll());
    }

    @Override
    public void setKey(Long categoryId, String... keys) {
        leavingKeyService.saveKey(categoryId, keys);
    }

    @Override
    public void setKey(String categoryName, String... keys) {
        this.setKey(leavingCategoryService.getCategoryIdByName(categoryName), keys);
    }

    @Override
    public void setCategory(Long categoryId, String... keys) {
        this.setKey(categoryId, keys);
        this.setCategory();
    }

    @Override
    public void setCategory(String categoryName, String... keys) {
        this.setKey(categoryName, keys);
        this.setCategory();
    }

    @Override
    public void setAllCategory(Long categoryId, String... keys) {
        this.setKey(categoryId, keys);
        this.setAllCategory();
    }

    @Override
    public void setAllCategory(String categoryName, String... keys) {
        this.setKey(categoryName, keys);
        this.setAllCategory();
    }

    @Override
    public void setCategory(List<LeavingMessage> messageList) {
        this.setCategory(leavingCategoryService.getAll(), messageList);
    }

    @Override
    public void setCategory(List<LeavingMessage> messageList, Long categoryId) {
        List<LeavingCategory> categories = new ArrayList<LeavingCategory>() {
            {
                add(leavingCategoryService.getLeavingById(categoryId));
            }
        };
        this.setCategory(categories, messageList);
    }

    @Override
    public void setCategory(List<LeavingMessage> messageList, String categoryName) {
        this.setCategory(messageList, leavingCategoryService.getCategoryIdByName(categoryName));
    }

    @Override
    public void setCategory(List<LeavingMessage> messageList, Long categoryId, String... keys) {
        this.setKey(categoryId, keys);
        this.setCategory(messageList);
    }

    @Override
    public void setCategory(List<LeavingMessage> messageList, String categoryName, String... keys) {
        this.setCategory(messageList, leavingCategoryService.getCategoryIdByName(categoryName), keys);
    }

    @Override
    public void setCategory(List<LeavingCategory> leavingCategories, LeavingMessage message) {
        String text = message.getMessageDetail();

        if (text == null || text.length() < 10) return;

        List<Long> categoryIds = new ArrayList<>();

        for (LeavingCategory category : leavingCategories) {
            Long categoryId = category.getId();
            //每个类别包含的字典
            Set<String> wordSet = leavingKeyService.getKeysByCategoryId(categoryId);
            if (wordSet.size() == 0) continue;

            List<String> list = KWUtil.getWords(text, wordSet);

            if (list.size() != 0) {
                categoryIds.add(categoryId);
            }
        }
        if (categoryIds.size() != 0) {
            //出现次数最多的类别Id
            Long maxCategoryId = Collections.max(categoryIds);
            leavingMessageService.updateCategory(message.getId(), maxCategoryId);
        }
    }

    @Override
    public void setCategory(List<LeavingCategory> leavingCategories, List<LeavingMessage> messages) {
        if (leavingCategories.size() == 0 || messages.size() == 0) return;
        for (LeavingMessage message : messages) {
            this.setCategory(leavingCategories, message);
        }
    }

    @Override
    public void delCategory() {
        leavingMessageService.deleteAllCategory();
    }

    @Override
    public void delKey() {
        leavingKeyService.deleteCategory();
    }

    @Override
    public void delKey(Long categoryId) {
        leavingKeyService.deleteCategory(categoryId);
    }

    @Override
    public void saveReport() {
        List<LeavingCategory> leavingCategories = leavingCategoryService.getAll();
        for (LeavingCategory leavingCategory : leavingCategories) {
            Long num = leavingMessageDao.countByLeavingCategoryIdAndIsCategory(leavingCategory.getId(), true);
            leavingCategory.setNumber(num);
            leavingCategoryService.updateCategoryById(leavingCategory);
            System.out.println(num);
        }
    }
}
