package com.example.wisdom.service.impl;

import com.example.wisdom.config.ResultBody;
import com.example.wisdom.dao.LeavingCategoryDao;
import com.example.wisdom.dao.LeavingMessageDao;
import com.example.wisdom.entity.LeavingCategory;
import com.example.wisdom.entity.LeavingMessage;
import com.example.wisdom.exception.BizException;
import com.example.wisdom.service.LeavingMessageService;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @description: LeavingMessageServiceImpl
 * @author: longxin
 * @date: 2020/4/30 10:47
 */
@Service
public class LeavingMessageServiceImpl implements LeavingMessageService {


    @Autowired
    private LeavingMessageDao leavingMessageDao;

    @Autowired
    private LeavingCategoryDao leavingCategoryDao;


    @Override
    public ResultBody list(String messageSubject, Pageable pageable) {
        if (!StringUtils.isEmpty(messageSubject)) {
            String searchContent = '%' + messageSubject + '%';
            return ResultBody.success(leavingMessageDao.findByMessageSubjectIsLike(searchContent, pageable));
        }
        return ResultBody.success(leavingMessageDao.findAll(pageable));
    }

    @Override
    public ResultBody list(Long categoryId, Pageable pageable) {
        if (StringUtils.isEmpty(categoryId)) {
            throw new BizException("-1", "类别id不能为空！");
        }
        LeavingMessage leavingMessage = new LeavingMessage();
        leavingMessage.setLeavingCategoryId(categoryId);
        Example<LeavingMessage> leavingMessageExample = Example.of(leavingMessage);

        String categoryName = leavingCategoryDao.findById(categoryId).get().getCategoryName();
        return ResultBody.success(leavingMessageDao.findAll(leavingMessageExample, pageable), categoryName);
    }

    @Override
    public List<LeavingMessage> getAll() {
        return leavingMessageDao.findAll();
    }

    @Override
    public List<LeavingMessage> getNotCategory() {
        return leavingMessageDao.findByisCategory(false);
    }

    @Override
    public List<LeavingMessage> getByCategory(boolean isCategory) {
        return leavingMessageDao.findByisCategory(isCategory);
    }

    @Override
    public void save(List<LeavingMessage> leavingMessages) {
        leavingMessageDao.saveAll(leavingMessages);
    }

    @Override
    public void deleteAllCategory() {
        leavingMessageDao.updateCategory();
    }

    @Override
    public void deleteAllIsCategory() {
        leavingMessageDao.updateisCategory();
    }

    @Override
    public void updateCategory(Long id, Long categoryId) {
        leavingMessageDao.updateCategory(id, categoryId);
    }


    /**
     * 个根据id查找某一个详情
     *
     * @param id
     */
    @Override
    public ResultBody findById(Long id) {
        if (StringUtils.isEmpty(id)) {
            throw new BizException("-1", "类别id不能为空！");
        }
        Optional<LeavingMessage> leavingMessage = leavingMessageDao.findById(id);

        if (StringUtils.isEmpty(leavingMessage.get().getLeavingCategoryId())) {
            return ResultBody.success(leavingMessage, "NO CATEGORY");
        }
        LeavingCategory leavingCategory = leavingCategoryDao.findById(leavingMessage.get().getLeavingCategoryId()).get();
        String categoryName = leavingCategory.getCategoryName();

        return ResultBody.success(leavingMessage, categoryName);
    }

    @Override
    public ResultBody updateCategoryLikeNumById(Long id) {
        if (StringUtils.isEmpty(id)) {
            ResultBody.error("参数传输错误");
        }
        LeavingMessage leavingMessage = leavingMessageDao.findById(id).get();
        leavingMessage.setId(id);
        leavingMessage.setLikeNumber(leavingMessage.getLikeNumber() + 1);
        leavingMessageDao.save(leavingMessage);
        return ResultBody.success();
    }

    @Override
    public ResultBody updateCategoryOppositionNumById(Long id) {
        if (StringUtils.isEmpty(id)) {
            ResultBody.error("参数传输错误");
        }
        LeavingMessage leavingMessage = leavingMessageDao.findById(id).get();
        leavingMessage.setId(id);
        leavingMessage.setOppositionNumber(leavingMessage.getOppositionNumber() + 1);
        leavingMessageDao.save(leavingMessage);
        return ResultBody.success();
    }

    @Override
    public ResultBody updateCategoryReplyOpinionById(Long id, String replyOpinion) {
        if (StringUtils.isEmpty(id)) {
            ResultBody.error("参数传输错误");
        }
        LeavingMessage leavingMessage = leavingMessageDao.findById(id).get();
        leavingMessage.setReplyOpinion(replyOpinion);
        leavingMessageDao.save(leavingMessage);
        return ResultBody.success();
    }

    @Override
    public List<LeavingMessage> getExcel(MultipartFile file) throws Exception {
        List<LeavingMessage> leavingMessages = new ArrayList<>();

        //1.得到上传的表
        Workbook workbook2 = WorkbookFactory.create(file.getInputStream());
        //2、获取表
        //指定表
        //Sheet sheet2 = workbook2.getSheet("210214473");
        //第一个表
        Sheet sheet2 = workbook2.getSheetAt(0);
        //获取表的总行数
        int num = sheet2.getLastRowNum();
        //System.out.println(num);
        //总列数
        int col = sheet2.getRow(0).getLastCellNum();


        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        //遍历excel每一行

        for (int j = 1; j <= num; j++) {
            LeavingMessage leavingMessage = new LeavingMessage();
            Row row1 = sheet2.getRow(j);

            //如果单元格中有数字或者其他格式的数据，则调用setCellType()转换为string类型
            Cell cell1 = row1.getCell(0);
            cell1.setCellType(CellType.STRING);
            //获取表中第i行，第2列的单元格
            Cell cell2 = row1.getCell(1);
            cell2.setCellType(CellType.STRING);
            //excel表的第i行，第3列的单元格
            Cell cell3 = row1.getCell(2);
            cell3.setCellType(CellType.STRING);
            Cell cell4 = row1.getCell(3);
            cell4.setCellType(CellType.STRING);
            Cell cell5 = row1.getCell(4);
            cell5.setCellType(CellType.STRING);

            leavingMessage.setId((long) Integer.parseInt(cell1.getStringCellValue()));
            leavingMessage.setUserName(cell2.getStringCellValue());
            leavingMessage.setMessageSubject(cell3.getStringCellValue());
            leavingMessage.setMessageTime(formatter.parse(cell4.getStringCellValue()));
            leavingMessage.setReplyOpinion(cell5.getStringCellValue());


            leavingMessages.add(leavingMessage);

            //这里new 一个对象，用来装填从页面上传的Excel数据，字段根据上传的excel决定
            //System.out.println(cell1 + "\t" + cell2 + "\t" + cell3 + "\t" + cell4 + "\t" + cell5);
        }
        return leavingMessages;
    }
}
