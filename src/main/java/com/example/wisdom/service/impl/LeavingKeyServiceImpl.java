package com.example.wisdom.service.impl;

import com.example.wisdom.dao.LeavingKeyDao;
import com.example.wisdom.entity.LeavingKey;
import com.example.wisdom.service.LeavingKeyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @description: LeavingKeyServiceImpl
 * @author: liupeng
 * @date: 2020/4/30 10:39
 */
@Service
public class LeavingKeyServiceImpl implements LeavingKeyService {
    @Autowired
    private LeavingKeyDao leavingKeyDao;


    @Override
    public List<LeavingKey> getAll() {
        return leavingKeyDao.findAll();
    }

    @Override
    public List<LeavingKey> getLeavingKeysByCategoryId(Long id, List<LeavingKey> leavingKeys) {
        List<LeavingKey> keyList = new ArrayList<>();
        for (LeavingKey leavingKey : leavingKeys) {
            if (id.equals(leavingKey.getId())) {
                keyList.add(leavingKey);
            }
        }

        return keyList;
    }

    @Override
    public Set<LeavingKey> getLeavingKeysByCategoryId(Long id) {
        return leavingKeyDao.findLeavingKeysByLeavingCategoryId(id);
    }

    @Override
    public Set<String> getKeysByCategoryId(Long id) {
        Set<String> names = new HashSet<String>();
        for (LeavingKey leavingKey : leavingKeyDao.findLeavingKeysByLeavingCategoryId(id)){
            names.add(leavingKey.getKeyName());
        }
        return names;
    }

    @Override
    public Set<String> getKeysByCategoryId(Long id, List<LeavingKey> leavingKeys) {
        Set<String> names = new HashSet<String>();
        for (LeavingKey leavingKey : getLeavingKeysByCategoryId(id, leavingKeys)){
            names.add(leavingKey.getKeyName());
        }
        return names;
    }

    @Override
    public void saveKey(Long categoryId, String... keys) {
        for (String key : keys) {
            if (leavingKeyDao.findByKeyName(key).size() != 0) {
                continue;
            }
            LeavingKey leavingKey = new LeavingKey();
            leavingKey.setKeyName(key);
            leavingKey.setLeavingCategoryId(categoryId);
            leavingKeyDao.save(leavingKey);
        }
    }

    @Override
    public void deleteCategory() {
        leavingKeyDao.deleteAll();
    }

    @Override
    public void deleteCategory(Long categoryId) {
        leavingKeyDao.deleteAllByLeavingCategoryId(categoryId);
    }

}
