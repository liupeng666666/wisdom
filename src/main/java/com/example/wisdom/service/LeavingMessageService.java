package com.example.wisdom.service;

import com.example.wisdom.config.ResultBody;
import com.example.wisdom.entity.LeavingMessage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

/**
 * @description: LeavingMessageService
 * @author: longxin
 * @date: 2020/4/30 10:51
 */
public interface LeavingMessageService {


    /**
     * @description: 获取分页列表
     * @param: pageable
     * @return:
     */
    ResultBody list(String messageSubject,Pageable pageable);

    /**
     * @description: 根据类别id获取分页列表
     * @param: pageable
     * @return:
     */
    ResultBody list(Long categoryId,Pageable pageable);

    /**
     * 获取全部留言
     * @return
     */
    List<LeavingMessage> getAll();

    /**
     * 获取未分类的留言
     * @return
     */
    List<LeavingMessage> getNotCategory();

    /**
     * 按需获取是否分类的留言
     * @return
     */
    List<LeavingMessage> getByCategory(boolean isCategory);

    /**
     * 保存信息
     * @param leavingMessages
     */
    void save(List<LeavingMessage> leavingMessages);

    /**
     * 将所有是否分类置位否
     */
    void deleteAllIsCategory();

    /**
     * 将已分类的置为否并清空
     */
    void deleteAllCategory();

    /**
     * 直接分类 - 将某个类容分类
     * @param id
     * @param categoryId
     */
    void updateCategory(Long id, Long categoryId);

    /**
     * 个根据id查找某一个详情
     * @param id
     */
    ResultBody findById(Long id);

    /**
     * 个根据id进行点赞
     * @param id
     */
    ResultBody updateCategoryLikeNumById(Long id);

    /**
     * 个根据id进行反对
     * @param id
     */
    ResultBody updateCategoryOppositionNumById(Long id);

    /**
     * 个根据id进行答复
     * @param id
     */
    ResultBody updateCategoryReplyOpinionById(Long id,String replyOpinion);

    List<LeavingMessage> getExcel(MultipartFile file) throws Exception;
}
