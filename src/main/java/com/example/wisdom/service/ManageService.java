package com.example.wisdom.service;

import com.example.wisdom.entity.LeavingCategory;
import com.example.wisdom.entity.LeavingMessage;

import java.util.List;

/**
 * @description: ManageService
 * @author: longxin
 * @date: 2020/4/30 10:50
 */
public interface ManageService {

    /**
     * 数据库字典分类，已分类的不再分类
     */
    void setCategory();

    /**
     * 数据库字典分类，已分类的将从新分类
     */
    void setAllCategory();

    /**
     * 设置字典 - 精确
     * @param categoryId
     * @param keys
     */
    void setKey(Long categoryId, String...keys);
    /**
     * 设置字典 - 模糊
     * @param categoryName
     * @param keys
     */
    void setKey(String categoryName, String...keys);

    /**
     * 新增分类的字典并分类未分类的 - 精确
     * @param categoryId
     * @param keys
     */
    void setCategory(Long categoryId, String...keys);

    /**
     * 新增分类的字典并分类未分类的 - 模糊
     * @param categoryName
     * @param keys
     */
    void setCategory(String categoryName, String...keys);

    /**
     *  新增数据字典的同时 全部分类 - 精确
     * @param categoryId
     * @param keys
     */
    void setAllCategory(Long categoryId, String...keys);

    /**
     *  新增数据字典的同时 全部分类 - 模糊
     * @param categoryName
     * @param keys
     */
    void setAllCategory(String categoryName, String...keys);


    /**
     * 对指定list进行分类
     * @param messageList
     */
    void setCategory(List<LeavingMessage> messageList);

    /**
     * 对指定list在指定分类中分类 - 精确
     * @param messageList
     * @param categoryId
     */
    void setCategory(List<LeavingMessage> messageList, Long categoryId);

    /**
     * 对指定list在指定分类中分类 - 模糊
     * @param messageList
     * @param categoryName
     */
    void setCategory(List<LeavingMessage> messageList, String categoryName);

    /**
     * 对指定内容在指定关键字和分类中分类 - 精确
     * @param messageList
     * @param categoryId
     * @param keys
     */
    void setCategory(List<LeavingMessage> messageList, Long categoryId, String...keys);

    /**
     * 对指定内容在指定关键字和分类中分类 - 模糊
     * @param messageList
     * @param categoryName
     * @param keys
     */
    void setCategory(List<LeavingMessage> messageList, String categoryName, String...keys);

    /**
     *  对指定单个内容分类
     * @param leavingCategories
     * @param message
     */
    void setCategory(List<LeavingCategory> leavingCategories, LeavingMessage message);

    /**
     *  对指定集合内容批量分类
     * @param leavingCategories
     * @param messages
     */
    void setCategory(List<LeavingCategory> leavingCategories, List<LeavingMessage> messages);

    /**
     * 删除所有内容的分类，分类状态也会被置为未分类
     */
    void delCategory();

    /**
     * 删除所有分类的字典
     */
    void delKey();

    /**
     * 删除指定分类的字典
     * @param categoryId
     */
    void delKey(Long categoryId);

    /**
     * 生成类别报表
     * @param
     */
    void saveReport();
}
