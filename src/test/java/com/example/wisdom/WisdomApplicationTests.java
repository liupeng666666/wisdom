package com.example.wisdom;

import com.example.wisdom.dao.LeavingMessageDao;
import com.example.wisdom.service.LeavingCategoryService;
import com.example.wisdom.service.LeavingMessageService;
import com.example.wisdom.service.ManageService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WisdomApplication.class)
class WisdomApplicationTests {
    //TODO 分类效果取决于关键字是否精准，关键字的数量等

    @Autowired
    private LeavingMessageService leavingMessageService;

    @Autowired
    private ManageService manageService;



    /**
     * 自动根据已有字典分类，已分类的不再分类
     */
    @Test
    void setCategory() {
        manageService.setCategory();
    }

    /**
     * 自动根据已有字典全部从新分类
     */
    @Test
    void setAllCategory() {
        manageService.setAllCategory();
    }

    /**
     * 新增字典后并全部从新分类
     */
    @Test
    void setAllCategoryAndSetKey() {
        manageService.setKey(2L, "党","务", "政", "政党", "政治建设");
        manageService.setKey(3L, "国","土", "资", "源", "资源", "资源建设");
        manageService.setKey(3L, "国","土", "资", "源", "资源", "资源建设");
        manageService.setKey(4L, "环","境", "保", "护", "环境", "保护", "爱护", "", "环境保护");
        manageService.setAllCategory();
    }



    /**
     * 新增分类的字典并分类，已分类的不再分类
     */
    @Test
    void testSetCategory() {
        // 1 对应分类ID， keys 包含改分类的多个关键字
        manageService.setCategory(1L, "城","乡", "建", "设");
        //重复的key不会二次录入
    }

    /**
     * 新增分类字典并全部再次分类，
     */
    @Test
    void testSetAllCategory() {
        // 1 对应分类ID， keys 包含改分类的多个关键字
        manageService.setAllCategory(1L, "城","乡", "建", "设");
    }


    /**
     * 删除所有内容的分类，测试使用
     */
    @Test
    void delAllCategory() {
        manageService.delCategory();
    }

    /**
     * 删除对应分类的字典，不传参全部删除，请谨慎操作
     */
    @Test
    void delKey() {
        manageService.delKey(2L);
    }


    @Test
    void testSetCategory1() {
//        Long num = leavingMessageDao.countByLeavingCategoryIdAndIsCategory(55203L, true);
//        System.out.println(num);
        manageService.saveReport();
    }

    @Test
    void testSetCategory2() {
        Pageable pageable = PageRequest.of(0, 5);
        leavingMessageService.list(14L, pageable);
    }

    @Test
    void testSetCategory3() {
        leavingMessageService.updateCategoryOppositionNumById(65992L);
    }

    @Test
    void testSetCategory4() {
    }

    @Test
    void testSetCategory5() {
    }
}
